#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'LadyHavoc'
SITENAME = 'LadyHavoc\'s DarkPlaces Quake'
SITEURL = ''

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'GMT'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = 'feeds/all.atom.xml'
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
# LINKS = (('Pelican', 'http://getpelican.com/'),
#          ('Python.org', 'http://python.org/'),
#          ('Jinja2', 'http://jinja.pocoo.org/'),
#          ('You can modify those links in your config file', '#'),)
LINKS = (
	('Icculus', 'http://icculus.org/'),
	('Wikipedia Affiliate', 'http://wikimediafoundation.org/wiki/Support_Wikipedia/en'),
)

# Social widget
SOCIAL = (
	# ('Another social link', '#'),
)

DEFAULT_PAGINATION = 5

THEME = 'theme'

STATIC_PATHS = ['static',]

#
# News
#
INDEX_SAVE_AS = 'news/index.html'
ARTICLE_URL = 'news/{date:%Y}/{date:%m}/{date:%d}.html'
ARTICLE_SAVE_AS = 'news/{date:%Y}/{date:%m}/{date:%d}.html'
YEAR_ARCHIVE_SAVE_AS = 'news/{date:%Y}/index.html'
MONTH_ARCHIVE_SAVE_AS = 'news/{date:%Y}/{date:%m}/index.html'

USE_FOLDER_AS_CATEGORY = False
DISPLAY_CATEGORIES_ON_MENU = False

#
# Pages
#
DISPLAY_PAGES_ON_MENU = True
PAGE_URL = '{slug}/'
PAGE_SAVE_AS = '{slug}/index.html'
SLUGIFY_SOURCE = 'basename'  # Use the filename as the slug source

#
# Tags and Category path
#
TAG_URL = 'news/tag/{slug}'
TAG_SAVE_AS = 'news/tag/{slug}/index.html'
TAGS_SAVE_AS = 'news/tags/index.html'

#
# Plugins
#
PLUGIN_PATHS = ['plugins',]
PLUGINS = [
	'toc',
	'neighbors'
]

MARKDOWN = {
	'extension_configs': {
		'markdown.extensions.extra': {},
		'markdown.extensions.meta': {},
	},
}
TOC = {
	'TOC_HEADERS': '^h[1-2]',
	'TOC_INCLUDE_TITLE': 'false',
	'TOC_FORMATTER': 'html5',
}

AUTHORS = {
	'LadyHavoc': 'ladyhavoc.png',
	'CloudWalk': 'cloudwalk.png'
}
