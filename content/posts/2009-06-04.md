Title: 2009-06-04
Date: 2009-06-04
Tags: dpmaster

New dpmaster master server release from Mathieu Olivier, version 2.0 brings the following features of interest to game developers:

- IPv6 support.

- Logging system.

- Game type filters in server list queries (and ability to restrict supported games in the master server, may be of interest to game teams hosting their own master server).

- Updated documentation and many fixes and improvements.

- (Note: dpmaster is only of interest to game development teams who have a game engine that supports the Quake3(r) master server protocol, or the extended dpmaster protocol, it is not of interest to users)
