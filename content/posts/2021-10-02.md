Title: 2021-10-02
Date: 2021-10-02
Tags: darkplaces, engine, chat, irc, github
Author: CloudWalk

Status update:

DarkPlaces engine is alive and well. We're working on a new release with the help of an additional developer. In other news, we now have an [official DarkPlaces Discord server](https://discord.com/invite/ZHT9QeW) for the community, bridged to the existing [#darkplaces IRC](irc://irc.anynet.org/darkplaces). We've moved the code repository to [GitHub](https://github.com/DarkPlacesEngine/darkplaces) due to popular demand as well. No ETA on a new release yet but we're shaking the bugs out of it.
