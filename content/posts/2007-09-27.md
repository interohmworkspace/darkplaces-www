Title: 2007-09-27
Date: 2007-09-27

Rygel's 2.7GB ultra texture pack is amazing, be sure to set gl_texturecompression 1 before installing it however, it may not even load otherwise, and it takes about 3 minutes to load the first level!