Title: 2010-01-13
Date: 2010-01-13
Tags: hmap2

New hmap2 (q1bsp compiler) release with the following changes:


Fixed rotating door compilation, it was generating corrupt hull data for player and shambler collisions, now works perfectly.  (To see this in action in your own q1 maps, try making a "func_wall" entity and setting the "origin" key to the center of rotation you want, then set "nextthink" "999999999" and "avelocity" "0 90 0" to see it spin 90 degrees per second on yaw and push you around)