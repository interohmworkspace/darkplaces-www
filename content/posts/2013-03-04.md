Title: 2013-03-04
Date: 2013-03-04
Tags: darkplaces, engine

New darkplaces update:

- Fix a crash on OpenGL 2.0 (DX9-class) video cards where glGetUniformBlockIndex is NULL, this was not properly guarded with an extension check, OpenGL 3.1 or higher drivers (DX10-class) have this function.