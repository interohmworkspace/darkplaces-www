Title: 2008-07-15
Date: 2008-07-15
Tags: dpmodel

New dpmodel build with fixed md3 normals (they were being calculated incorrectly - apparently Quake3 uses a strange latitude/longitude angle format instead of pitch/yaw), this utility converts .smd model files (saved by HalfLife export plugins for various modeling programs) to .dpm and .md3 model formats.