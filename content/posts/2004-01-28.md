Title: 2004-01-28
Date: 2004-01-28
Tags: darkplaces, engine

romi has completed his [.rtlights](http://www.kgbsyndicate.com/romi/) file collection for Quake Mission Pack 1: Scourge of Armagon (also known as hipnotic), and for those not aware of his Quake .rtlights file collection as well, I recommend it :)

romi also has some videos in the works to show his lighting creations better than the screenshots do.

(If anyone doesn't remember, to experience an rtlights file it must be placed in the maps directory, make sure in the Video Options menu that you are in 32bit color mode (sorry this can't be done on a 3Dfx Voodoo1/2/3/Rush/Banshee), load up the map you want, and then simply type r_shadow_realtime_world 1 in the console, enjoy.  Maps without rtlights files are often slower and less colorful.)
