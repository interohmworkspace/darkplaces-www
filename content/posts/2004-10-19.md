Title: 2004-10-19
Date: 2004-10-19
Tags: darkplaces, engine

New darkplaces release, what's new:

- Fixed a number of problems with scoreboard updates (names/colors/frags) in the server.

- Fixed duplicate name bug in client mini-scoreboard (the one to the right of the statusbar in multiplayer).

- Tomaz enlarged particle font from 256x256 to 512x512 and added some code to allow saving it to a .tga (and an example one has been posted on the download page).

- Quitting without using the "quit" command in the console now disconnects from the server, and properly kicks off everyone on a local server.

- Credited romi and |Rain| for their contributions in the readme.

- Added 32bit color support to SDL builds.



