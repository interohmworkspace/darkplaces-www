Title: 2011-06-26
Date: 2011-06-26
Tags: darkplaces, engine

New DarkPlaces engine release to fix the following issues with the previous release:

- Really fixed the solid water bug on dedicated servers this time.