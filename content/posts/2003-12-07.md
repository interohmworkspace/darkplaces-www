Title: 2003-12-07
Date: 2003-12-07
Tags: darkplaces, engine

Another new engine release (20031207b):

- Added `r_wateralpha` cvar to the effects options menu.

New engine release:

- No longer limits framerate to 20fps while in console because I got so many complaints about this (no REALLY that was not a bug like everyone assumes, that was a feature, just a very misunderstood one), so now it only drops to 20fps when not the active window (it already did this).

- Changed hardware gamma handling in glx to no longer turn on/off based on mouse being in the window or not, but simply whether it is the active window.

- Fixed 16bit video modes in Linux support. (oww this was broken, I only use 32bit :)

- Fixed a bug with console parsing that existed in almost all versions of quake except quakeworld by switching to the qwcl COM_Parse for console parsing (in english: fixed connect commands involving a port, like 127.0.0.1:26000), thanks very much to Fuh for mentioning this bug.

- Removed need for gfx/menuplyr.lmp, some old unused code required it, no idea why that was still there (in english: this has no importance to quake players, only modders making standalone stuff).
