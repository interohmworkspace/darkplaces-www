Title: 2012-12-22
Date: 2012-12-22
Tags: darkplaces, engine, hmap2, dpmod

New darkplaces update for the survivors of the end of the world:

- support for X360 gamepad (`joy_enable 1` required).

- `-quoth` support.

- fixed hip1m1 gold key door bug.

- fixed some bugs with effectinfo.txt parsing that could cause effects to not be properly initialized.

- sound() builtin now supports speed control (pitch shifting) and 128 channels with snd_channel*volume cvars controlling their volume level independently (make weapon sounds louder/quieter compared to other sound types, etc).

- reduced memory usage (sounds are now resampled during mixing rather than at load time).

- better support for static (non-animated) iqm models.

- added `dpshaderkillifcvar` keyword in q3 shader files which allows alternative shaders to be used based on cvars (games can use this to swap in different kinds of water shader among other things).

- removed `r_hdr` cvar (use `r_viewfbo 2` instead).

- multiple bugs with `vid_sRGB 1` have been fixed.

- made mdl rendering mode faithful to software Quake by removing an unwanted half-pixel texcoord offset.

- added cvars `r_nearest_2d` and `r_nearest_conchars` which let you decide whether to use nearest filtering on the entire 2D UI or just the console font.

- support for BSP2 format (modified Quake bsp with higher limits) to match the new hmap2 feature.

- fix missing runes on the hud (due to previously bugged detection of hipnotic/rogue qc code).

- fixed playback of intro demos in Malice game.

- fixed a bug where playing back demos and changing slowmo cvar would not immediately take effect.

- New hmap2 release bringing support for much more complex maps (beyond Quake limits), it is much less likely to crash on complex maps now and will write BSP2 format if necessary.

- New dpmod release with some minor fixes.