title: Help
summary: Installation and technical assistance
banner: e3m1.jpg

## Installation

### Windows

All that DarkPlaces needs from the Quake CD is pak files (*do not* copy opengl32.dll from the Quake CD, it will not work with DarkPlaces!). With this in mind, all you need to do is make a Quake directory, extract the darkplaces engine zip to that directory, then make a quake/id1 directory, and put the pak0.pak and pak1.pak from your Quake CD into the quake/id1 directory, then all should be well.

#### DOS Quake CD ####

Try to use the DOS Quake installer if you can, using [DOSBox](http://dosbox.sourceforge.net) if necessary, then copy the pak0.pak and pak1.pak files to your `id1` directory in the darkplaces installation.

#### WinQuake CD ####

Copy the `D:\Data\id1\pak*.pak` files to your id1 directory.

#### Linux Quake CD ####

Find an archiver (perhaps *7zip* or *winrar*) that can extract files from rpm archives, locate the pak files and copy them to your `id1` directory.

### Linux ###

All that DarkPlaces needs from the Quake CD is pak files, with this in mind, all you need to do is make a `~/quake` directory, extract the DarkPlaces engine zip to that directory, then make a `~/quake/id1` directory, and put the pak0.pak and pak1.pak from your Quake CD into the `~/quake/id1` directory, then all should be well, you will probably also want to make a `~/bin/darkplaces` script containing the following text:

```
#!/bin/sh
cd ~/quake
./darkplaces-sdl $*
```

Then do `chmod +x ~/bin/darkplaces`

For more information on Quake installation on Linux see [the Linux Quake How To](http://www.tldp.org/HOWTO/Quake-HOWTO.html).

#### DOS Quake CD ####

```
cat /media/cdrom/resource.001 /media/cdrom/resource.002 >quake.lha
unlha x quake.lha
```

If you can't get *unlha* or *lha* for your distribution, try using DOSBox to run the Quake installer.

#### WinQuake CD ####

Copy `/media/cdrom/data/id1/pak*.pak` to your `id1` directory.

#### Linux Quake CD ####

```
mkdir temp
cd temp
```

in the following line replace quake.rpm with a correct rpm filename

```
cat /media/cdrom/quake.rpm | rpm2cpio | cpio -i
```

Now you should have a mess of subdirectories, locate the pak files and copy to your `id1` directory.

Alternatively if you have an rpm-based distribution you could install the rpm, but it is easier to maintain your `~/quake` directory than `/usr/share/games/quake` so you may want to copy the `id1/pak*.pak` from there and uninstall the rpm.

### Mac OS X ###

All that DarkPlaces needs from the Quake CD is pak files, with this in mind, make a folder named Quake, drag the Darkplaces.app into this Quake folder, make a folder inside the Quake folder (alongside Darkplaces) named id1, and put the pak0.pak and pak1.pak from your Quake CD into the quake/id1 directory, then all should be well, simply run the Darkplaces app

#### DOS Quake CD ####

Unknown. Linux solution may work if you can get hold of lha, otherwise use DOSBox to run the Quake installer.

#### WinQuake CD ####

Find the `data` folder on the cdrom and copy the `data/id1/pak*.pak` files to your `id1` folder.

#### Linux Quake CD ####

Unknown. If you can get hold of rpm2cpio and cpio you should be able to follow the Linux method.
