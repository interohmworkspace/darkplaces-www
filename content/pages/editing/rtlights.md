title: RTLights
summary: How to create and modifying the realtime light entities in a level.
banner: e2m5.jpg
status: hidden
url: editing/rtlights/
save_as: editing/rtlights/index.html
template: editing
icon: <svg style="color: var(--bs-yellow);" width="16" height="16" fill="currentColor" class="bi bi-lightbulb-fill" viewBox="0 0 16 16"><path d="M2 6a6 6 0 1 1 10.174 4.31c-.203.196-.359.4-.453.619l-.762 1.769A.5.5 0 0 1 10.5 13h-5a.5.5 0 0 1-.46-.302l-.761-1.77a2 2 0 0 0-.453-.618A5.98 5.98 0 0 1 2 6m3 8.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1l-.224.447a1 1 0 0 1-.894.553H6.618a1 1 0 0 1-.894-.553L5.5 15a.5.5 0 0 1-.5-.5"/></svg>

## About

What are rtlights and how are they used by DarkPlaces.

## Lights

Details about list entities and what the properties are.

## Editing

The commands for editing light entities in a level and saving.

## Cubemaps

Walkthrough on creating a cubemap and then applying it to a light.
