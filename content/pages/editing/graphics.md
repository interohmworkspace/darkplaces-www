title: Graphics
summary: Updating and creating new graphics and media
banner: e2m2.jpg
status: hidden
url: editing/
save_as: editing/index.html
template: editing
icon: <svg style="color: var(--bs-green);" width="16" height="16" fill="currentColor" class="bi bi-palette-fill" viewBox="0 0 16 16"><path d="M12.433 10.07C14.133 10.585 16 11.15 16 8a8 8 0 1 0-8 8c1.996 0 1.826-1.504 1.649-3.08-.124-1.101-.252-2.237.351-2.92.465-.527 1.42-.237 2.433.07M8 5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3m4.5 3a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3M5 6.5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0m.5 6.5a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3"/></svg>



## Replacing Content

Formats supported:

- TGA (recommended)
- PNG (loads very slowly)
- JPG (loads slowly)
- PCX
- WAL
- Imp

Usually you want to put replacement content in either `id1/` or another directory such as `pretty/` inside your quake directory. In DarkPlaces you can run multiple `-game` options at once (such as `-game ctf -game pretty -game dpmod` to have texture overrides from *pretty*, maps from *ctf*, and game-play from *dpmod*) or multiple game-dirs specified with the `gamedir` console command (`gamedir ctf pretty dpmod`).

All texture layers are optional except diffuse (the others *are not loaded* without it)


## Skins

Skins are the textures for models. Any model texture can be overridden with a file with the same name as the model file plus `_0.tga`. It also supports texture variants.

`./progs/player.mdl_0.tga`
:   diffuse

`./progs/player.mdl_0_norm.tga`
:   normalmap (can have alpha channel with bumpmap height for offsetmapping/reliefmapping)

`./progs/player.mdl_0_bump.tga`
:   bumpmap (not loaded if normalmap is present)

`./progs/player.mdl_0_gloss.tga`
:   gloss map, also known as specular color map (the color of reflected light)

`./progs/player.mdl_0_glow.tga`
:   glow map

`./progs/player.mdl_0_pants.tga`
:   pants image, greyscale and does not cover the same pixels as the diffuse texture (this is additive blended ('Screen' mode in Photoshop) on top of the diffuse texture with a color tint according to your pants color), make sure you have black pixels in the diffuse image otherwise you get 'ghosting' (washed out and too bright because both layers are being combined on these pixels)

`./progs/player.mdl_0_shirt.tga`
:   shirt image, same type as pants


## Textures in Specific Maps

`./textures/e1m1/ecop1_6.tga`
:   -

`./textures/e1m1/ecop1_6_norm.tga`
:   -

`./textures/e1m1/ecop1_6_bump.tga`
:   -

`./textures/e1m1/ecop1_6_gloss.tga`
:   -

`./textures/e1m1/ecop1_6_glow.tga`
:   -

`./textures/e1m1/ecop1_6_pants.tga`
:   pants and shirt layers are possible on bmodel entities with quakec modifications to set their .colormap field

`./textures/e1m1/ecop1_6_shirt.tga`
:   -



## Textures in All Maps

* `./textures/quake.tga`
* `./textures/quake_norm.tga`
* `./textures/quake_bump.tga`
* `./textures/quake_gloss.tga`
* `./textures/quake_glow.tga`
* `./textures/quake_pants.tga`
* `./textures/quake_shirt.tga`



## HUD and Menu Pictures

* `./gfx/conchars.tga`



## Models

Same as in Quake, you can replace a model with exactly the same file name (including file extension), so for example an MD3 player model has to be renamed `progs/player.mdl`, and a small box of shells in MD3 format has to be renamed `maps/b_shell0.bsp`



## Skin Files

How to make `.skin` files for multiple skins on a Quake3 (md3) or DarkPlacesModel (dpm) model

These files use the same format as the ones in Quake3 (except being named `modelname_0.skin`, `modelname_1.skin`, and so on), they specify what texture to use on each part of the md3 (or zym or dpm or psk) model, their contents look like the following:

Tag                                                | Description
-------------------------------------------------- | ----------------
`torso,progs/player_default.tga`                   | says that the model part named "torso" should use the image progs/player_default.tga
`gun,progs/player_default.tga`                     | says that the model part named "gun" should use the image progs/player_default.tga
`muzzleflash,progs/player_default_muzzleflash.tga` | says that the model part named "muzzleflash" should use the image progs/player_default_muzzleflash.tga - this is useful for transparent skin areas which should be kept separate from opaque skins
`tag_head,`                                        | says that the first tag is named "tag_head" - this is only useful for QuakeC mods using segmented player models so that they can look up/down without their legs rotating, don't worry about it as a user
`tag_torso,`                                       | second tag name
`tag_weapon,`                                      | third tag name



## Soundtrack in OGG Format

These files must be in OGG or WAV format, and numbers begin at 002 if you wish to replace (or install) the Quake CD music - since track 001 was the Quake data track.

File Path                               | Result
--------------------------------------- | ---------------------------------
`./sound/cdtracks/track002.ogg` | Replacement track for "cd loop 2"
`./sound/cdtracks/track003.ogg` | Replacement track for "cd loop 3"



## Example List of Filenames

File Path                                  | Result
------------------------------------------ | ------------------------------------
`./progs/player.mdl`               | replaces the player model
`./progs/player.mdl_0.skin`        | text file that specifies textures to use on an MD3 model) 
`./progs/player_default.tga`       | texture referenced by the `.skin`, make sure that any special parts of this are black, like pants should be black here otherwise you get pink pants when you use a red color in-game) 
`./progs/player_default_pants.tga` | white pants area of the skin, this is colored by the engine according to your color settings, additive blended (which is called "Screen" mode in Photoshop if you wish to preview the layers) 
`./progs/player_default_shirt.tga` | white shirt area of the skin, similar to pants described above)
`./progs/player_default_norm.tga`  | normalmap texture for player_default, alpha channel can contain a heightmap for offsetmapping (`r_glsl_offsetmapping 1` in console) to use, alternatively you can use `_bump.tga` instead of this which is only a heightmap and the engine will generate the normalmap for you) 
`./progs/player_default_gloss.tga` | glossmap (shiny areas) for `player_default`) 
`./progs/player_default_glow.tga`  | glowmap (glowing stuff) for `player_default`, this is full-brights and such, be sure the corresponding pixels are black in the `player_default.tga`, because just like pants/shirt this is additive blended) 
`./textures/quake.tga`             | replaces the quake logo on the arch in start.bsp)
`./textures/quake_norm.tga`        | same as for a player)
`./textures/quake_gloss.tga`       | same as for a player)
`./textures/#water1.tga`           | replaces *water1 texture in the maps, # is used instead of * in filenames)
`./gfx/conchars.tga`               | replacement font image, this was in `gfx.wad` in Quake) 
`./gfx/conback.tga`                | replacement console background, just like in Quake) 
`./gfx/mainmenu.tga`               | replacement main menu image, just like in Quake) 
`./maps/b_bh25.bsp`                | replacement for normal health pack, for example this could be an MD3 model instead) 
`./sound/cdtracks/track002.ogg`    | replacement track for "cd loop 2"
`./sound/cdtracks/track003.ogg`    | replacement track for "cd loop 3"
