title: Features
slug: features
url: engine/features/
save_as: engine/features/index.html
template: engine
summary: A summary of the DarkPlaces engine
banner: e1m2.jpg
status: hidden



## Graphics ##

* Redesigned effects including smoke, blood, bubbles and explosions.
* Better looking dynamic lights.
* External texture support (see Replacement Content section below)
* Realtime bumpmapped lighting/shadowing support (`r_shadow_realtime_world` cvar) with many options. (note: very slow if you do not have .rtlights files installed, be sure to get some from dpmod or search on the web for rtlights files)
* .rtlights file support (improves performance/appearance of realtime lighting)
* .rtlights file editing (see `r_editlights_help` in game)
* Alpha blended sprites (instead of glquake's masked sprites).
* Interpolated entity movement and animations (both models and sprites).
* Overbright and fullbright support on walls and models (like winquake).
* Colormapping support on any q1 model (like winquake).
* Fog (set with "fog density red green blue" command)
* Skybox (`loadsky "mtnsun_"` will load `"env/mtnsun_ft.tga"` and so on).
* Sky rendering improved (no more glquake distortion).
* Sky polygons obscure geometry just like in winquake.
* Color calibration menu to ensure a proper Quake experience.
* Improved model lighting (directional shading).
* No messy .ms2 model mesh files (no glquake dir anymore either).
* New improved crosshair (team color coded).
* Improved image loading (smoother menus and such).
* Ability to disable particle effects (`cl_particles*` cvars).
* Decals (`cl_decals` cvar to enable).
* Stainmaps (`cl_stainmap` cvar to enable).
* Sorted transparent stuff to render better.
* Improved multitexture support (`r_textureunits` (1-4 supported), needs `gl_combine 1` because of overbright)
* Improved chase cam (`chase_active 1` no longer goes into walls)
* More configurable console background (`scr_conalpha` and `scr_conbrightness`)
* Optional fullbrights (`r_fullbrights 0/1` followed by `r_restart`)
* Dynamic Farclip (no distance limits in huge maps)
* Improved `gl_flashblend` (now renders a corona instead of an ugly blob)
* DynamicLight coronas (more realism)
* Transparent statusbar (`sbar_alpha`) that does not block your view as much.
* No 8bit texture uploads (fixes 'green' walls in the distance).
* Fixed view blends (glquake was quite broken).
* JPEG texture support using libjpeg (Thanks Elric)
* Video Options, Color Control, and Effects Options menus added, and more options.
* .dlit file support (produced by `hmap2 -light`) for fast per-pixel lighting without shadowing.
* pointfile command is improved (for leak finding in maps when you have a .pnt file from a failed qbsp compile)
* configurable particle effects (effectinfo.txt, can be reloaded at any time by `cl_particles_reloadeffects` command for quick testing)
* fixed envmap command (makes a skybox of the current scene)


## Sound ##

* Ogg and wav file overrides for cd tracks (example: id1/sound/cdtracks/track002.ogg or .wav) (Thanks Elric)
* Streaming ogg sounds to save memory (Ogg sounds over a certain size are streamed automatically) (Thanks Elric)
* Ogg Vorbis sound support (all .wav sounds look for .ogg if the .wav is missing, useful for making mods smaller, particularly useful for cd tracks) (Thanks Elric)
* Stereo sound file support (useful for cd tracks)
* 7.1 surround sound mixing support (`snd_channels` cvar selects how many to use, default 2 for stereo)


## Client ##

* `showtime` cvar.
* `showdate` cvar.
* `-benchmark` option to run automated timedemo benchmarks (`-benchmark demo1` does `+timedemo demo1` and quits immediately when finished)
* timedemo automatically puts results in gamedir/benchmark.log
* Slightly improved aiming on quake servers (does not support proquake aiming).
* `-sndspeed samplerate` (default: 44100, quake used 11025)
* `snd_swapstereo` cvar (for people with backwards SB16 sound cards)
* Saves video settings to config and restores them properly
* Ability to change video settings during game (video options menu or `vid_*` cvars)
* `showfps` cvar.
* Sends 20fps network packets to improve modem play instead of one per frame. (sys_ticrate controls network framerate)
* Allow skin colormaps 14 and 15 (freaky 😀)
* Longer chat messages.
* No more 72fps limit, `cl_maxfps` lets you decide.
* Support for more mouse buttons (mouse1-mouse16, mwheelup/mwheeldown are aliases to mouse4 and mouse5).
* Server browser for public (`sv_public 1`) darkplaces servers as well as quakeworld servers.
* `log_file` cvar to log console messages to a file.
* `condump` command to dump recent console history to a file.
* PK3 archive support with compression support using zlib (Thanks Elric)
* `maps` command lists installed maps
* tab completion of map names on map/changelevel commands
* tab completion of rcon commands
* .ent file replacement allows you to modify sky and fog settings on a per-map basis (use `sv_saveentfile` command in singleplayer and then edit the worldspawn entity in a text editor, for example setting "sky" "mtnsun" to load the skybox mtnsun in your favorite maps, or "fog" "0.03 0.2 0.2 0.2" to put fog in the level)
* Switchable bindmaps (in_bind command allows you to bind keys in one of 8 bindmaps, 0-7, in_bindmap command allows you to select two active bindmaps to use at once, ones missing in the first are checked in the second)
* Options menu "Reset to Defaults" option works better than in Quake
* `cvarlist` and `cmdlist` commands
* improved tab completion of commands and cvars, with default value and description listed
* `curl` command (downloads a URL to a pk3 archive and loads it)
* `fs_rescan` command (allows you to load a newly installed pak/pk3 archive without quitting the game)
* `saveconfig` command (allows you to save settings before quitting the game, mostly useful when debugging the engine if you expect it to crash)
* `gamedir` command to change current mod (only works while disconnected).
* QuakeWorld support.
* ProQuake message macros (%l location, %d last death location, %h health, %a armor, %x rockets, %c cells, %t current time, %r rocket launcher status (I need RL, I need rockets, I have RL), %p powerup status (quad pent ring), %w weapon status (SSG:NG:SNG:GL:RL:LG).
* Support for ProQuake .loc files (locs/e1m1.loc or maps/e1m1.loc)
* Support for QIZMO .loc files (maps/e1m1.loc)
* Ingame editing of .loc files using locs_* commands (locs_save saves a new .loc file to maps directory)
* bestweapon command (takes a number sequence like bestweapon 87654321, digits corresponding to weapons, first ones are preferred over last ones, only uses weapons that have 1 ammo or more)
* `ls` and `dir` commands to list files in the Quake virtual filesystem (useful to find files inside paks)
* `toggle` command allows you to use a single bind to toggle a cvar between 0 and 1
* `slowmo` cvar allows you to pause/slow down/speed up demo playback (try using these binds for example: bind f1 "slowmo 0";bind f2 "slowmo 0.1";bind f3 "slowmo 1")
* AVI video recording using builtin I420 codec (`bind f4 "toggle cl_capturevideo"`), with automatic creation of sequentially numbered avi files for each recording session. (WARNING: HUGE files, make sure you have several gigabytes of disk space available! and it is only recommended during demo playback! You will probably want to reencode these videos using VirtualDub, mencoder, or other utilities before posting them on a website)
* Ping display in scoreboard, even on Quake servers (on DarkPlaces servers it also shows packet loss)


## Server ##

* Allows clients to connect through firewalls (automatic feature)
* Works behind firewalls unlike NetQuake (must port forward UDP packets on the relevant port from the firewall to the server, as with any game)
* More accurate movement and precise aiming.
* 255 player support.
* `sv_cheats` cvar controls cheats (no longer based on deathmatch).
* `slowmo` cvar controls game speed.
* No crash with the buggy 'teleport train' in shub's pit.
* Allow skin colormaps 14 and 15 (freaky :)
* `sys_ticrate` applies to listen (client) servers as well as dedicated.
* `sv_public` cvar to advertise to master server.
* `log_file` cvar to log console messages to a file.
* `condump` command to dump recent console history to a file.
* PK3 archive support with compression support using zlib (Thanks Elric)
* Option to prevent wallhacks from working (`sv_cullentities_trace 1`).
* Selectable protocol (`sv_protocolname QUAKE` for example allows quake clients to play, default is `sv_protocolname DP7` or a later protocol)
* Automatic file downloads to DarkPlaces clients.
* Ability to send URLs to DarkPlaces clients to download pk3 archives needed to play on this server.
* rcon support for remote administration by trusted clients with matching `rcon_password`, or external quakeworld rcon tools
* `prvm_edictset`, `prvm_global`, `prvm_globals`, and `prvm_globalset` commands aid in QuakeC debugging
* `prvm_printfunction` function prints out the QuakeC assembly opcodes of a function, can be useful if you can't decompile the progs.dat file
* `prvm_profile` command gives call count and estimated builtin-function cost
* `sys_colortranslation` cvar controls processing of Quake3-style `^` color codes in terminal output, default is white text on windows and ANSI color output on Linux/Mac OSX
* `sys_specialcharactertranslation` controls processing of special Quake characters to make colored names more readable in terminal output


## Modding ##

* HalfLife map support (place your HalfLife wads in quake/id1/textures/ or quake/MODDIR/textures/ as the maps need them)
* Larger q1 and hl map size of +-32768 units.
* Colored lighting (.lit support) for q1 maps.
* Q3 map support (no shaders though), with no limits.
* Q2 and Q3 model support, with greatly increased limits (256 skins, 65536 frames, 65536 vertices, 65536 triangles). (Note: md2 player models are not supported because they have no skin list)
* Optimized QuakeC interpreter so mods run faster.
* Bounds checking QuakeC interpreter so mods can't do naughty things with memory.
* Warnings for many common QuakeC errors.
* Unprecached models are now a warning (does not kill the server anymore).
* External texture support (see dpextensions.qc DP_GFX_EXTERNALTEXTURES).
* Fog ("fog" key in worldspawn, same parameters as fog command).
* .spr32 and halflife .spr sprites supported. (Use Krimzon's tool to make spr32, and lhfire can render directly to spr32, or just use replacement textures on .spr).
* Skybox ("sky" key in worldspawn, works like loadsky and quake2).
* Stereo wav sounds supported.
* Ogg Vorbis sounds supported. (Thanks Elric)
* ATTN_NONE sounds are no longer directional (good for music).
* `play2 sound` testing command (ATTN_NONE variant of play).
* `r_texturestats` and `memstats` and `memlist` commands to give memory use info.
* Lighting on sprites (put ! anywhere in sprite filename to enable).
* More `r_speeds` info (now a transparent overlay instead of spewing to console).
* Supports rotating bmodels (use avelocity, and keep in mind the bmodel needs the "origin" key set to rotate (read up on hipnotic rotation support in your qbsp docs, or choose another qbsp if yours does not support this feature), or in q3 maps an origin brush works).
* More sound channels.
* More dynamic lights (32 changed to 256).
* More precached models and sounds (256 changed to 4096).
* Many more features documented in dpextensions.qc. (bullet tracing on models, qc player input, etc)


## Thanks ##

* David Cristian for contributing the mod browser menu.
* Rudolf 'divverent' Polzer for contributions related to Nexuiz.
* Dresk for contributing modding features.
* SavageX for maintaining the "DarkPlaces bugs affecting Nexuiz" wiki page.
* Tomaz for adding features, fixing many bugs, and being generally helpful.
* Andreas 'Black' Kirsch for much work on the QuakeC VM (menu.dat, someday clprogs.dat) and other contributions.
* Mathieu 'Elric' Olivier for much work on the sound engine (especially the Ogg vorbis support)
* MoALTz for some bugfixes and cleanups
* Joseph Caporale for adding 5 mouse button support.
* KGB|romi for his contributions to the Quake community, including his rtlights project and many suggestions, his id1 romi_rtlights.pk3 is included in darkplaces mod releases.
* Zombie for making great levels and general DarkPlaces publicity.
* FrikaC for FrikQCC and FrikBot and general community support.
* Transfusion Project for recreating Blood in the world of Quake.
* de-we for the great icons.
* |Rain| for running my favorite anynet IRC server and his bot feh (which although a bit antisocial never seems to grow tired of being my calculator).
* VorteX for the DP_QC_GETTAGINFO extension.
* Ludwig Nussel for the `~/.games/darkplaces/` user directory support on non-Windows platforms (allowing games to be installed in a non-writable system location as is the standard on UNIX but still save configs to the user's home directory).

